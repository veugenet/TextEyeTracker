﻿using System;
using Tobii.EyeTracking.IO;
using System.Runtime.Serialization;

namespace EyeTracker.Core
{
    [DataContract(Namespace = "")]
    public class TrackPoint
    {
        [DataMember]
        public Int64 Time { get; private set; }
        [DataMember]
        public Point2D RightGazePoint { get; private set; }
        [DataMember]
        public Point2D LeftGazePoint { get; private set; }
        [DataMember]
        public float RightPupilDiameter { get; private set; }
        [DataMember]
        public float LeftPupilDiameter { get; private set; }
        [DataMember]
        public Point3D RightGazePoint3D { get; private set; }
        [DataMember]
        public Point3D LeftGazePoint3D { get; private set; }

        public TrackPoint(IGazeDataItem gazeData, Int64 time)
        {
            RightGazePoint = gazeData.RightGazePoint2D;
            LeftGazePoint = gazeData.LeftGazePoint2D;
            Time = time;
            LeftPupilDiameter = gazeData.LeftPupilDiameter;
            RightPupilDiameter = gazeData.RightPupilDiameter;
            LeftGazePoint3D = gazeData.LeftGazePoint3D;
            RightGazePoint3D = gazeData.RightGazePoint3D;
        }

        public bool Validate()
        {
            if (RightGazePoint.X == -1 && RightGazePoint.Y == -1)
                if (LeftGazePoint.X == -1 && LeftGazePoint.Y == -1)
                    return false;
            return true;
        }

        public override string ToString()
        {
            return "Right: " + RightGazePoint.X + " , " + RightGazePoint.Y + " ; "+
                    "Left: " + LeftGazePoint.X + " , " + LeftGazePoint.Y + " ; " +
                    "Time: " + Time;
        }
    }
}
