﻿using System;
using System.Xml.Serialization;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Xml;
using System.Drawing;
using System.Windows.Forms;

namespace EyeTracker.Core
{
    [DataContract(Namespace="")]
    public class SessionProperties
    {
        [XmlIgnore]
        public Font TextFont {get; set;}

        [DataMember]
        [XmlElement(ElementName = "Font")]
        public string FontString
        {
            get
            {
                if (TextFont != null)
                {
                    TypeConverter converter = TypeDescriptor.GetConverter(typeof(Font));

                    return converter.ConvertToString(this.TextFont);
                }
                else return null;
            }
            set
            {
                TypeConverter converter = TypeDescriptor.GetConverter(typeof(Font));

                this.TextFont = (Font)converter.ConvertFromString(value);
            }
        }

        public Color BackgroungColor { get; set; }
        public Color TextColor { get; set; }

        [XmlIgnore]
        private String outputFolder;
        [XmlIgnore]
        public String OutputFolder
        { 
            get
            {
                return outputFolder;
            }
            set
            {
                if (value != null && !value.Equals(string.Empty))
                {
                    String str = value;
                    if (str[str.Length - 1] != '\\' && str[str.Length - 1] != '/')
                        str += '\\';
                    outputFolder = str;
                }
            }
        }
        [XmlIgnore]
        public String OutputFilePrefix { get; set; }
        [XmlIgnore]
        public String TextFile { get; set; }

        [DataMember]
        public Padding TextPadding { get; set; }
        //[DataMember]
        public TrackType.TrackEye TrackType { get; set; }
        [DataMember]
        public int LineHeight { get; set; }

        //[XmlIgnore]
        //public Clock Clock { get; set; }

        public SessionProperties()
        {
            TextFont = new Font("Calibri", 24);
            BackgroungColor = Color.Black;
            TextColor = Color.Black;
            TrackType = EyeTracker.TrackType.TrackEye.LeftEye;
        }
    }
}
