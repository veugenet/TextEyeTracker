﻿using System;

namespace EyeTracker
{
    public class TrackType
    {
        public enum TrackEye { None, BothEye, LeftEye, RightEye };
        private TrackEye type;
        public TrackType(TrackEye t)
        {
            type = t;
        }

        public override String ToString()
        {
            switch(type)
            {
                case TrackEye.None:
                    return "None";
                case TrackEye.BothEye:
                    return "Both eyes";
                case TrackEye.LeftEye:
                    return "Left eye";
                case TrackEye.RightEye:
                    return "Right eye";
            }
            return "";
        }

        public TrackEye GetTrackEye()
        {
            return type;
        }
    }
}
