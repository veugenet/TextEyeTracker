﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Drawing;
using System.Windows.Forms;

namespace EyeTracker.Core
{

    public struct PageInfo
    {
        public int StartWord;
        public int EndWord;
    }

    public class TextFormer
    {
        private Bitmap bmp;
        private Graphics graphics;
        private Font font;
        private Size areaSize;

        private List<Word> wordList = new List<Word>();
        public List<Word> WordList
        {
            get
            {
                List<Word> wl = new List<Word>(wordList);
                return wl;
            }
            private set
            {
                wordList = value;
            }
        }

        private List<PageInfo> pageInfo = new List<PageInfo>();
        public List<PageInfo> PageInfo
        {
            get
            {
                List<PageInfo> res = new List<PageInfo>(pageInfo);
                return res;
            }
            private set
            {
                pageInfo = value;
            }
        }

        private List<String> lines = new List<string>();
        public List<String> TextLines
        {
            get
            {
                List<string> res = new List<string>(lines);
                return res;
            }
            private set
            {
                lines = value;
            }
        }

        public int LineHeight { get; private set; }
        public Padding PagePadding { get; private set; }

        public TextFormer(String textFileName, Font font, Size areaSize, Padding pagePadding, int lineHeight)
        {
            bmp = new Bitmap(100, 100);
            graphics = Graphics.FromImage(bmp);
            this.font = font;
            this.areaSize = areaSize;
            this.PagePadding = pagePadding;
            this.LineHeight = lineHeight;

            try
            {
                String line;
                using (StreamReader sr = new StreamReader(textFileName))
                {
                    while ((line = sr.ReadLine()) != null)
                    {
                        lines.Add(line);
                    }
                }
                form();
            }
            catch
            {
               
            }
        }

        public TextFormer(List<Word> words, List<PageInfo> pages, Size areaSize, Padding pagePadding, int lineHeight)
        {
            WordList = words;
            PageInfo = pages;
            PagePadding = pagePadding;
            LineHeight = lineHeight;
            this.areaSize = areaSize;
        }

        private void form()
        {
            PointF currentPos = new PointF();
            currentPos.X = PagePadding.Left;
            currentPos.Y = PagePadding.Top;
            int wordNumber = 1;
            int lineNumber = 1;
            PageInfo pg;
            pg.StartWord = 1;
            int wordID = 0;

            foreach(String l in lines)
            {
                String[] words = l.Split(' ');
                
                foreach(String w in words)
                {
                    SizeF wordSize = graphics.MeasureString(w, font);
                    if (wordSize.Width + currentPos.X >= areaSize.Width - PagePadding.Right)
                    {
                        currentPos.X = PagePadding.Left;
                        lineNumber++;
                        currentPos.Y = (lineNumber - 1) * font.Height + PagePadding.Top + (lineNumber - 1) * LineHeight;
                    }
                    if (wordSize.Height + currentPos.Y >= areaSize.Height - PagePadding.Bottom)
                    {
                        currentPos.X = PagePadding.Left;
                        currentPos.Y = PagePadding.Top;
                        lineNumber = 1;
                        pg.EndWord = wordNumber - 1;
                        pageInfo.Add(pg);
                        pg.StartWord = wordNumber;
                    }

                    Rectangle rc = new Rectangle((int)currentPos.X, (int)currentPos.Y, (int)wordSize.Width, (int)wordSize.Height);
                    Word word = new Word(wordID, w, rc);
                    wordID++;

                    currentPos.X += wordSize.Width;

                    wordList.Add(word);
                    wordNumber++;
                }

                lineNumber++;
                currentPos.X = PagePadding.Left;
                currentPos.Y = (lineNumber - 1) * font.Height + PagePadding.Top + (lineNumber - 1) * LineHeight;
            }
            pg.EndWord = wordNumber - 1;
            pageInfo.Add(pg);
        }
    }
}
