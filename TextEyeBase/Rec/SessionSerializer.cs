﻿using System;
using System.Text;
using System.Runtime.Serialization;
using System.Xml;
using System.IO;

namespace EyeTracker.Recorder
{
    public class SessionSerializer
    {
        public static bool SerializeSession(SessionRecorder recorder, String fileName)
        {
            NetDataContractSerializer ser = new NetDataContractSerializer();
            var settings = new XmlWriterSettings { Indent = true, Encoding = Encoding.UTF8 };
            try {
                using (var w = XmlWriter.Create(fileName, settings))
                {
                    ser.WriteStartObject(w, recorder);
                    w.WriteAttributeString("xmlns", "eyet", null, "http://schemas.datacontract.org/2004/07/Tobii.EyeTracking.IO");
                    w.WriteAttributeString("xmlns", "rect", null, "http://schemas.datacontract.org/2004/07/System.Drawing");
                    ser.WriteObjectContent(w, recorder);
                    ser.WriteEndObject(w);
                }
            } catch { return false; }
            return true;
        }

        public static SessionRecorder DeserializeSession(String fileName)
        {
            try {
                NetDataContractSerializer ser = new NetDataContractSerializer();
                FileStream fs = new FileStream(fileName, FileMode.Open);
                XmlDictionaryReader reader = XmlDictionaryReader.CreateTextReader(fs, new XmlDictionaryReaderQuotas());
                SessionRecorder recorder = (SessionRecorder)ser.ReadObject(reader);
                return recorder;
            }
            catch
            {
                return null;
            }
            
        }
    }
}
