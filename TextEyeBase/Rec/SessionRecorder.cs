﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using EyeTracker.Core;

namespace EyeTracker.Recorder
{
    [DataContract(Namespace="")]
    public class SessionRecorder
    {

        [DataMember]
        public List<TrackPoint> Points { get; set; }
        [DataMember]
        public List<Word> Words { get; set; }
        [DataMember]
        public List<PageInfo> PageInfo { get; set; }
        [DataMember]
        public SessionProperties SessionProps {get; set;}
        [DataMember]
        public Int64 StartClockTime { get; set; }
        [DataMember]
        public System.Drawing.Rectangle ScreenResolution { get; set; }
        [DataMember]
        public Dictionary<Int64, int> PageTimer { get; set; }

        public SessionRecorder()
        {
            Points = new List<TrackPoint>();
            ScreenResolution = System.Windows.Forms.Screen.PrimaryScreen.Bounds;
            PageTimer = new Dictionary<Int64, int>();
        }

        public void AddPont(TrackPoint point)
        {
            if (point.Validate())
                Points.Add(point);
        }

    }
}
