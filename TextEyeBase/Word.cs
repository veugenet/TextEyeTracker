﻿using System;
using System.Drawing;
using System.Runtime.Serialization;

namespace EyeTracker.Core
{
    [DataContract(Namespace="")]
    public class Word
    {
        [DataMember]
        public String ThisWord { get; private set; }
        [DataMember()]
        public Rectangle Coord { get; private set; }
        [DataMember]
        public int ID { get; private set; }

        public Word(int id, String word, Rectangle coord)
        {
            ThisWord = word;
            Coord = coord;
            ID = id;
        }

    }
}
