﻿using System;
using System.Windows.Forms;
using Tobii.EyeTracking.IO;
using System.IO;
using System.Drawing;
using EyeTracker.Calib;
using EyeTracker.Recorder;
using EyeTracker.Core;
using EyeTracker;
using EyeTracker.Datasource;

namespace eyeTracker
{
    public partial class SetupForm : Form
    {

        private readonly EyeTrackerBrowser trackerBrowser;
        private SessionProperties sessionProps = new SessionProperties();

        private IDataSource connectedTracker;
        private string connectionName;
        private readonly Clock clock;

        public SetupForm()
        {
            InitializeComponent();
            trackerBrowser = new EyeTrackerBrowser();
            trackerBrowser.EyeTrackerFound += EyetrackerFound;
            clock = new Clock();
            cmbEyeTracker.Items.Add("From sample");
            TrackType tt1 = new TrackType(TrackType.TrackEye.BothEye);
            TrackType tt2 = new TrackType(TrackType.TrackEye.LeftEye);
            TrackType tt3 = new TrackType(TrackType.TrackEye.RightEye);
            TrackType tt4 = new TrackType(TrackType.TrackEye.None);
            cmbTrackType.Items.Add(tt4);
            cmbTrackType.Items.Add(tt1);
            cmbTrackType.Items.Add(tt2);
            cmbTrackType.Items.Add(tt3);
        }

        private void EyetrackerFound(object sender, EyeTrackerInfoEventArgs e)
        {
            EyeTrackerItem item = new EyeTrackerItem(e);
            cmbEyeTracker.Items.Add(item);
        }

        private void SetupForm_Load(object sender, EventArgs e)
        {
            trackerBrowser.StartBrowsing();
            cmbTrackType.SelectedItem = 0;
            readSettings();
        }

        private void connectToEyeTracker(EyeTrackerInfo info)
        {
            try
            {
                connectedTracker = new DataSourceEyeTracker(info, clock, sessionProps.TrackType);
                connectionName = info.ProductId;
            }
            catch (EyeTrackerException ee)
            {
                if (ee.ErrorCode == 0x20000402)
                {
                    MessageBox.Show("Failed to upgrade protocol. " +
                        "This probably means that the firmware needs" +
                        " to be upgraded to a version that supports the new sdk.", "Upgrade Failed", MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                }
                else
                {
                    MessageBox.Show("Eyetracker responded with error " + ee, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                DisconnectTracker();
            }
            catch (Exception)
            {
                MessageBox.Show("Could not connect to eyetracker.", "Connection Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                DisconnectTracker();
            }
        }

        private void loadFromSample()
        {
            if (openRecDlg.ShowDialog() == DialogResult.OK)
            {
                SessionRecorder recorder = SessionSerializer.DeserializeSession(openRecDlg.FileName);
                if (recorder == null)
                {
                    MessageBox.Show("Could not load from sample.", "Loading Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    connectedTracker = new DataSourceSample(recorder, clock);
                    connectionName = openRecDlg.FileName;
                }
            }
        }

        private void DisconnectTracker()
        {
            if (connectedTracker != null)
            {
                connectedTracker.Disconnect();
                connectionName = string.Empty;
                connectedTracker = null;
            }
        }

        private void startSession(IDataSource tracker)
        {
            try
            {
                if (tracker.GetDataSourceType() == DataSourceTypeEnum.EYETRACKER)
                {
                    sessionProps.OutputFilePrefix = edFilePrefix.Text;
                    sessionProps.TrackType = ((TrackType)cmbTrackType.SelectedItem).GetTrackEye();
                    //sessionProps.Clock = clock;
                    Size size = new Size(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
                    TextFormer textFormer = new TextFormer(sessionProps.TextFile, sessionProps.TextFont, size, sessionProps.TextPadding, sessionProps.LineHeight);
                    TextForm experimentForm = new TextForm(connectedTracker, sessionProps, textFormer);
                    experimentForm.ShowDialog();
                }
                if (tracker.GetDataSourceType() == DataSourceTypeEnum.SIMULATION)
                {
                    SessionRecorder sr = tracker.GetOwnProperties();
                    SessionProperties sp = sr.SessionProps;
                    //sp.Clock = clock;
                    sp.BackgroungColor = sessionProps.BackgroungColor;
                    sp.TextColor = sessionProps.TextColor;
                    sp.TrackType = (TrackType.TrackEye)cmbTrackType.SelectedIndex;
                    Size size = new Size(sr.ScreenResolution.Width, sr.ScreenResolution.Height);
                    TextFormer textFormer = new TextFormer(sr.Words, sr.PageInfo, size, sp.TextPadding, sp.LineHeight);
                    TextForm experimentForm = new TextForm(connectedTracker, sp, textFormer);
                    experimentForm.ShowDialog();
                }
            }
            catch (ArgumentException e)
            {
                System.Diagnostics.Debug.Write(e.StackTrace);
            }
            catch (Exception e)
            {
                MessageBox.Show(null, "You not have access to output directory!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                System.Diagnostics.Debug.Write(e.StackTrace);
            }
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            if (connectedTracker == null)
            {
                MessageBox.Show(null, "You don't connect to eye tracker!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if (connectedTracker.GetDataSourceType() == DataSourceTypeEnum.SIMULATION)
            {
                startSession(connectedTracker);
                return;
            }

            if (!File.Exists(sessionProps.TextFile))
            {
                MessageBox.Show(null, "You don't select text file!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (!Directory.Exists(sessionProps.OutputFolder))
            {
                MessageBox.Show(null, "You don't select output directory!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (sessionProps.TextFont == null)
            {
                MessageBox.Show(null, "You don't set text font!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            startSession(connectedTracker);
        }

        private void SetupForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            DisconnectTracker();
            trackerBrowser.StopBrowsing();
            saveSetings();
        }

        private void btnColorDlg_Click(object sender, EventArgs e)
        {
            colorDialog.Color = sessionProps.BackgroungColor;
            if (colorDialog.ShowDialog() == DialogResult.OK)
                sessionProps.BackgroungColor = colorDialog.Color;
        }

        private void btnFontDlg_Click(object sender, EventArgs e)
        {
            fontDialog.Font = sessionProps.TextFont;
            if (fontDialog.ShowDialog() == DialogResult.OK)
                sessionProps.TextFont = fontDialog.Font;
        }

        private void btnText_Click(object sender, EventArgs e)
        {
            openFileDialog.FileName = sessionProps.TextFile;
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                sessionProps.TextFile = openFileDialog.FileName;
                updateUI();
            }
        }

        private void readSettings()
        {
            sessionProps.TextFile = Properties.Settings.Default.TextFile;
            sessionProps.TextFont = Properties.Settings.Default.TextFont;
            sessionProps.BackgroungColor = Properties.Settings.Default.BackgroundColor;
            sessionProps.TextColor = Properties.Settings.Default.TextColor;
            sessionProps.OutputFolder = Properties.Settings.Default.OutputFolder;
            sessionProps.TrackType = (TrackType.TrackEye)Properties.Settings.Default.TrackType;
            sessionProps.TextPadding = Properties.Settings.Default.TextPadding;
            sessionProps.LineHeight = Properties.Settings.Default.LineHeight;
            updateUI();
        }

        private void saveSetings()
        {
            Properties.Settings.Default.TextFile = sessionProps.TextFile;
            Properties.Settings.Default.TextFont = sessionProps.TextFont;
            Properties.Settings.Default.BackgroundColor = sessionProps.BackgroungColor;
            Properties.Settings.Default.TextColor = sessionProps.TextColor;
            Properties.Settings.Default.OutputFolder = sessionProps.OutputFolder;
            Properties.Settings.Default.TrackType = (int)sessionProps.TrackType;
            Properties.Settings.Default.TextPadding = sessionProps.TextPadding;
            Properties.Settings.Default.LineHeight = sessionProps.LineHeight;
            Properties.Settings.Default.Save();
        }

        private void updateUI() 
        {
            btnTextFile.Text = "Text file: " + Path.GetFileName(sessionProps.TextFile);
            btnOutputFolder.Text = "Output folder: " + sessionProps.OutputFolder;
            if (connectedTracker == null)
            {
                btnConnect.Text = "Connect";
                cmbEyeTracker.Enabled = true;
            }
            else
            {
                btnConnect.Text = "Disconnect";
                cmbEyeTracker.Enabled = false;
            }
            cmbTrackType.SelectedIndex = (int)sessionProps.TrackType;
        }

        private void btnFontColorDlg_Click(object sender, EventArgs e)
        {
            colorDialog.Color = sessionProps.TextColor;
            if (colorDialog.ShowDialog() == DialogResult.OK)
                sessionProps.TextColor = colorDialog.Color;
        }

        private void btnPageDlg_Click(object sender, EventArgs e)
        {
            PaddingDialog padDlg = new PaddingDialog();
            padDlg.SetPadding(sessionProps.TextPadding);
            padDlg.SetLineHeight(sessionProps.LineHeight);
            padDlg.ShowDialog();
            if (padDlg.Accept)
            {
                sessionProps.TextPadding = padDlg.GetPadding();
                sessionProps.LineHeight = padDlg.GetLineHeight();
            }
            padDlg = null;
        }

        private void btnOutputFolder_Click(object sender, EventArgs e)
        {
            folderBrowserDialog.SelectedPath = sessionProps.OutputFolder;
            if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
                sessionProps.OutputFolder = folderBrowserDialog.SelectedPath;
            updateUI();
        }

        private void btnCalibrate_Click(object sender, EventArgs e)
        {
            if (connectedTracker == null)
            {
                MessageBox.Show(null, "You don't connect to eye tracker!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (cmbEyeTracker.SelectedItem == null)
            {
                MessageBox.Show(null, "You don't select eye tracker!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (connectedTracker.GetDataSourceType() == DataSourceTypeEnum.SIMULATION)
            {
                MessageBox.Show(null, "Calibration for simulation is imposible.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            var runner = new CalibrationRunner();

            try
            {
                // Start a new calibration procedure
                Calibration result = this.connectedTracker.Calibrate();
                if (result != null)
                {
                    var resultForm = new CalibrationResultForm();
                    resultForm.SetPlotData(result);
                    resultForm.ShowDialog();
                }
                else
                {
                    MessageBox.Show("Not enough data to create a calibration (or calibration aborted).");
                }
            }
            catch
            {

            }
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            if (connectedTracker != null)
            {
                DisconnectTracker();
                updateUI();
                
                return;
            }
            if (cmbEyeTracker.SelectedItem == null)
            {
                MessageBox.Show(null, "You don't select eye tracker!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (!cmbEyeTracker.SelectedItem.ToString().Equals("From sample"))
            {
                connectToEyeTracker((cmbEyeTracker.SelectedItem as EyeTrackerItem).GetInfo());
            }
            else
            {
                loadFromSample();
            }
            updateUI();
        }

        private void cmbTrackType_SelectedIndexChanged(object sender, EventArgs e)
        {
            sessionProps.TrackType = (TrackType.TrackEye)cmbTrackType.SelectedIndex;
        }
    }
}
