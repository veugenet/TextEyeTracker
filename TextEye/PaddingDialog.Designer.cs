﻿namespace eyeTracker
{
    partial class PaddingDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblPaddingLeft = new System.Windows.Forms.Label();
            this.lblPaddingRight = new System.Windows.Forms.Label();
            this.numPadLeft = new System.Windows.Forms.NumericUpDown();
            this.numPadRight = new System.Windows.Forms.NumericUpDown();
            this.numPadBottom = new System.Windows.Forms.NumericUpDown();
            this.numPadTop = new System.Windows.Forms.NumericUpDown();
            this.lblPaddingBottom = new System.Windows.Forms.Label();
            this.lblPaddingTop = new System.Windows.Forms.Label();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.numLineHeight = new System.Windows.Forms.NumericUpDown();
            this.lblLineHeight = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numPadLeft)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPadRight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPadBottom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPadTop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numLineHeight)).BeginInit();
            this.SuspendLayout();
            // 
            // lblPaddingLeft
            // 
            this.lblPaddingLeft.AutoSize = true;
            this.lblPaddingLeft.Location = new System.Drawing.Point(12, 9);
            this.lblPaddingLeft.Name = "lblPaddingLeft";
            this.lblPaddingLeft.Size = new System.Drawing.Size(66, 13);
            this.lblPaddingLeft.TabIndex = 1;
            this.lblPaddingLeft.Text = "Padding left:";
            // 
            // lblPaddingRight
            // 
            this.lblPaddingRight.AutoSize = true;
            this.lblPaddingRight.Location = new System.Drawing.Point(111, 9);
            this.lblPaddingRight.Name = "lblPaddingRight";
            this.lblPaddingRight.Size = new System.Drawing.Size(72, 13);
            this.lblPaddingRight.TabIndex = 3;
            this.lblPaddingRight.Text = "Padding right:";
            // 
            // numPadLeft
            // 
            this.numPadLeft.Location = new System.Drawing.Point(15, 25);
            this.numPadLeft.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numPadLeft.Name = "numPadLeft";
            this.numPadLeft.Size = new System.Drawing.Size(81, 20);
            this.numPadLeft.TabIndex = 4;
            // 
            // numPadRight
            // 
            this.numPadRight.Location = new System.Drawing.Point(114, 25);
            this.numPadRight.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numPadRight.Name = "numPadRight";
            this.numPadRight.Size = new System.Drawing.Size(81, 20);
            this.numPadRight.TabIndex = 5;
            // 
            // numPadBottom
            // 
            this.numPadBottom.Location = new System.Drawing.Point(114, 71);
            this.numPadBottom.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numPadBottom.Name = "numPadBottom";
            this.numPadBottom.Size = new System.Drawing.Size(81, 20);
            this.numPadBottom.TabIndex = 9;
            // 
            // numPadTop
            // 
            this.numPadTop.Location = new System.Drawing.Point(15, 71);
            this.numPadTop.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numPadTop.Name = "numPadTop";
            this.numPadTop.Size = new System.Drawing.Size(81, 20);
            this.numPadTop.TabIndex = 8;
            // 
            // lblPaddingBottom
            // 
            this.lblPaddingBottom.AutoSize = true;
            this.lblPaddingBottom.Location = new System.Drawing.Point(111, 55);
            this.lblPaddingBottom.Name = "lblPaddingBottom";
            this.lblPaddingBottom.Size = new System.Drawing.Size(84, 13);
            this.lblPaddingBottom.TabIndex = 7;
            this.lblPaddingBottom.Text = "Padding bottom:";
            // 
            // lblPaddingTop
            // 
            this.lblPaddingTop.AutoSize = true;
            this.lblPaddingTop.Location = new System.Drawing.Point(12, 55);
            this.lblPaddingTop.Name = "lblPaddingTop";
            this.lblPaddingTop.Size = new System.Drawing.Size(67, 13);
            this.lblPaddingTop.TabIndex = 6;
            this.lblPaddingTop.Text = "Padding top:";
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(21, 146);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 10;
            this.btnOk.Text = "OK";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(114, 146);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 11;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // numLineHeight
            // 
            this.numLineHeight.Location = new System.Drawing.Point(15, 120);
            this.numLineHeight.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numLineHeight.Name = "numLineHeight";
            this.numLineHeight.Size = new System.Drawing.Size(81, 20);
            this.numLineHeight.TabIndex = 12;
            // 
            // lblLineHeight
            // 
            this.lblLineHeight.AutoSize = true;
            this.lblLineHeight.Location = new System.Drawing.Point(12, 104);
            this.lblLineHeight.Name = "lblLineHeight";
            this.lblLineHeight.Size = new System.Drawing.Size(62, 13);
            this.lblLineHeight.TabIndex = 13;
            this.lblLineHeight.Text = "Line height:";
            // 
            // PaddingDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(207, 176);
            this.Controls.Add(this.lblLineHeight);
            this.Controls.Add(this.numLineHeight);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.numPadBottom);
            this.Controls.Add(this.numPadTop);
            this.Controls.Add(this.lblPaddingBottom);
            this.Controls.Add(this.lblPaddingTop);
            this.Controls.Add(this.numPadRight);
            this.Controls.Add(this.numPadLeft);
            this.Controls.Add(this.lblPaddingRight);
            this.Controls.Add(this.lblPaddingLeft);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PaddingDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Padding";
            this.Shown += new System.EventHandler(this.PaddingDialog_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.numPadLeft)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPadRight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPadBottom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPadTop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numLineHeight)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblPaddingLeft;
        private System.Windows.Forms.Label lblPaddingRight;
        private System.Windows.Forms.NumericUpDown numPadLeft;
        private System.Windows.Forms.NumericUpDown numPadRight;
        private System.Windows.Forms.NumericUpDown numPadBottom;
        private System.Windows.Forms.NumericUpDown numPadTop;
        private System.Windows.Forms.Label lblPaddingBottom;
        private System.Windows.Forms.Label lblPaddingTop;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.NumericUpDown numLineHeight;
        private System.Windows.Forms.Label lblLineHeight;
    }
}