﻿namespace eyeTracker
{
    partial class SetupForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SetupForm));
            this.btnStart = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnPageDlg = new System.Windows.Forms.Button();
            this.btnFontColorDlg = new System.Windows.Forms.Button();
            this.btnTextFile = new System.Windows.Forms.Button();
            this.btnFontDlg = new System.Windows.Forms.Button();
            this.btnColorDlg = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnConnect = new System.Windows.Forms.Button();
            this.cmbTrackType = new System.Windows.Forms.ComboBox();
            this.btnCalibrate = new System.Windows.Forms.Button();
            this.cmbEyeTracker = new System.Windows.Forms.ComboBox();
            this.colorDialog = new System.Windows.Forms.ColorDialog();
            this.fontDialog = new System.Windows.Forms.FontDialog();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.lblFilePrefix = new System.Windows.Forms.Label();
            this.edFilePrefix = new System.Windows.Forms.TextBox();
            this.btnOutputFolder = new System.Windows.Forms.Button();
            this.openRecDlg = new System.Windows.Forms.OpenFileDialog();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(93, 480);
            this.btnStart.Margin = new System.Windows.Forms.Padding(4);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(161, 42);
            this.btnStart.TabIndex = 2;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnPageDlg);
            this.groupBox2.Controls.Add(this.btnFontColorDlg);
            this.groupBox2.Controls.Add(this.btnTextFile);
            this.groupBox2.Controls.Add(this.btnFontDlg);
            this.groupBox2.Controls.Add(this.btnColorDlg);
            this.groupBox2.Location = new System.Drawing.Point(16, 137);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox2.Size = new System.Drawing.Size(333, 191);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Text";
            // 
            // btnPageDlg
            // 
            this.btnPageDlg.Location = new System.Drawing.Point(168, 76);
            this.btnPageDlg.Margin = new System.Windows.Forms.Padding(4);
            this.btnPageDlg.Name = "btnPageDlg";
            this.btnPageDlg.Size = new System.Drawing.Size(153, 46);
            this.btnPageDlg.TabIndex = 4;
            this.btnPageDlg.Text = "Set padding";
            this.btnPageDlg.UseVisualStyleBackColor = true;
            this.btnPageDlg.Click += new System.EventHandler(this.btnPageDlg_Click);
            // 
            // btnFontColorDlg
            // 
            this.btnFontColorDlg.Location = new System.Drawing.Point(168, 23);
            this.btnFontColorDlg.Margin = new System.Windows.Forms.Padding(4);
            this.btnFontColorDlg.Name = "btnFontColorDlg";
            this.btnFontColorDlg.Size = new System.Drawing.Size(153, 46);
            this.btnFontColorDlg.TabIndex = 3;
            this.btnFontColorDlg.Text = "Set font color";
            this.btnFontColorDlg.UseVisualStyleBackColor = true;
            this.btnFontColorDlg.Click += new System.EventHandler(this.btnFontColorDlg_Click);
            // 
            // btnTextFile
            // 
            this.btnTextFile.Location = new System.Drawing.Point(8, 129);
            this.btnTextFile.Margin = new System.Windows.Forms.Padding(4);
            this.btnTextFile.Name = "btnTextFile";
            this.btnTextFile.Size = new System.Drawing.Size(313, 46);
            this.btnTextFile.TabIndex = 2;
            this.btnTextFile.UseVisualStyleBackColor = true;
            this.btnTextFile.Click += new System.EventHandler(this.btnText_Click);
            // 
            // btnFontDlg
            // 
            this.btnFontDlg.Location = new System.Drawing.Point(8, 76);
            this.btnFontDlg.Margin = new System.Windows.Forms.Padding(4);
            this.btnFontDlg.Name = "btnFontDlg";
            this.btnFontDlg.Size = new System.Drawing.Size(153, 46);
            this.btnFontDlg.TabIndex = 1;
            this.btnFontDlg.Text = "Set font";
            this.btnFontDlg.UseVisualStyleBackColor = true;
            this.btnFontDlg.Click += new System.EventHandler(this.btnFontDlg_Click);
            // 
            // btnColorDlg
            // 
            this.btnColorDlg.Location = new System.Drawing.Point(8, 23);
            this.btnColorDlg.Margin = new System.Windows.Forms.Padding(4);
            this.btnColorDlg.Name = "btnColorDlg";
            this.btnColorDlg.Size = new System.Drawing.Size(153, 46);
            this.btnColorDlg.TabIndex = 0;
            this.btnColorDlg.Text = "Set backgroung color";
            this.btnColorDlg.UseVisualStyleBackColor = true;
            this.btnColorDlg.Click += new System.EventHandler(this.btnColorDlg_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnConnect);
            this.groupBox1.Controls.Add(this.cmbTrackType);
            this.groupBox1.Controls.Add(this.btnCalibrate);
            this.groupBox1.Controls.Add(this.cmbEyeTracker);
            this.groupBox1.Location = new System.Drawing.Point(16, 15);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(333, 102);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Eye tracker";
            // 
            // btnConnect
            // 
            this.btnConnect.Location = new System.Drawing.Point(221, 28);
            this.btnConnect.Margin = new System.Windows.Forms.Padding(4);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(100, 28);
            this.btnConnect.TabIndex = 7;
            this.btnConnect.Text = "Connect";
            this.btnConnect.UseVisualStyleBackColor = true;
            this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // cmbTrackType
            // 
            this.cmbTrackType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTrackType.FormattingEnabled = true;
            this.cmbTrackType.Location = new System.Drawing.Point(8, 64);
            this.cmbTrackType.Margin = new System.Windows.Forms.Padding(4);
            this.cmbTrackType.Name = "cmbTrackType";
            this.cmbTrackType.Size = new System.Drawing.Size(208, 21);
            this.cmbTrackType.TabIndex = 6;
            this.cmbTrackType.SelectedIndexChanged += new System.EventHandler(this.cmbTrackType_SelectedIndexChanged);
            // 
            // btnCalibrate
            // 
            this.btnCalibrate.Location = new System.Drawing.Point(221, 62);
            this.btnCalibrate.Margin = new System.Windows.Forms.Padding(4);
            this.btnCalibrate.Name = "btnCalibrate";
            this.btnCalibrate.Size = new System.Drawing.Size(100, 28);
            this.btnCalibrate.TabIndex = 5;
            this.btnCalibrate.Text = "Calibrate";
            this.btnCalibrate.UseVisualStyleBackColor = true;
            this.btnCalibrate.Click += new System.EventHandler(this.btnCalibrate_Click);
            // 
            // cmbEyeTracker
            // 
            this.cmbEyeTracker.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbEyeTracker.FormattingEnabled = true;
            this.cmbEyeTracker.Location = new System.Drawing.Point(8, 31);
            this.cmbEyeTracker.Margin = new System.Windows.Forms.Padding(4);
            this.cmbEyeTracker.Name = "cmbEyeTracker";
            this.cmbEyeTracker.Size = new System.Drawing.Size(208, 21);
            this.cmbEyeTracker.TabIndex = 4;
            // 
            // openFileDialog
            // 
            this.openFileDialog.Filter = "Text files|*.txt";
            this.openFileDialog.Title = "Select text file";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.lblFilePrefix);
            this.groupBox3.Controls.Add(this.edFilePrefix);
            this.groupBox3.Controls.Add(this.btnOutputFolder);
            this.groupBox3.Location = new System.Drawing.Point(16, 335);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox3.Size = new System.Drawing.Size(331, 137);
            this.groupBox3.TabIndex = 6;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Output";
            // 
            // lblFilePrefix
            // 
            this.lblFilePrefix.AutoSize = true;
            this.lblFilePrefix.Location = new System.Drawing.Point(5, 25);
            this.lblFilePrefix.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblFilePrefix.Name = "lblFilePrefix";
            this.lblFilePrefix.Size = new System.Drawing.Size(54, 13);
            this.lblFilePrefix.TabIndex = 8;
            this.lblFilePrefix.Text = "File prefix:";
            // 
            // edFilePrefix
            // 
            this.edFilePrefix.Location = new System.Drawing.Point(9, 44);
            this.edFilePrefix.Margin = new System.Windows.Forms.Padding(4);
            this.edFilePrefix.Name = "edFilePrefix";
            this.edFilePrefix.Size = new System.Drawing.Size(312, 20);
            this.edFilePrefix.TabIndex = 7;
            // 
            // btnOutputFolder
            // 
            this.btnOutputFolder.Location = new System.Drawing.Point(8, 76);
            this.btnOutputFolder.Margin = new System.Windows.Forms.Padding(4);
            this.btnOutputFolder.Name = "btnOutputFolder";
            this.btnOutputFolder.Size = new System.Drawing.Size(313, 46);
            this.btnOutputFolder.TabIndex = 6;
            this.btnOutputFolder.UseVisualStyleBackColor = true;
            this.btnOutputFolder.Click += new System.EventHandler(this.btnOutputFolder_Click);
            // 
            // openRecDlg
            // 
            this.openRecDlg.FileName = "Open recorded file";
            this.openRecDlg.Filter = "XML|*.xml";
            // 
            // SetupForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(365, 532);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnStart);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "SetupForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TET - Session properties";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SetupForm_FormClosing);
            this.Load += new System.EventHandler(this.SetupForm_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnFontDlg;
        private System.Windows.Forms.Button btnColorDlg;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnCalibrate;
        private System.Windows.Forms.ComboBox cmbEyeTracker;
        private System.Windows.Forms.ColorDialog colorDialog;
        private System.Windows.Forms.FontDialog fontDialog;
        private System.Windows.Forms.Button btnTextFile;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.Button btnFontColorDlg;
        private System.Windows.Forms.Button btnPageDlg;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label lblFilePrefix;
        private System.Windows.Forms.TextBox edFilePrefix;
        private System.Windows.Forms.Button btnOutputFolder;
        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.OpenFileDialog openRecDlg;
        private System.Windows.Forms.ComboBox cmbTrackType;
    }
}

