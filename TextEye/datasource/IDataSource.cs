﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tobii.EyeTracking.IO;

namespace EyeTracker.Datasource
{
    public interface IDataSource : IDisposable, IHasOwnProperties
    {
        void SubscribeListenerAndStartTracking(IListener Listener);

        void StopTrackingAndUnsubscribeListener(IListener Listener);

        void Disconnect();

        DataSourceTypeEnum GetDataSourceType();

        Calibration Calibrate();

        Clock GetClock();
    }
}
