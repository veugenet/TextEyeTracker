﻿using System;
using System.Threading;
using Tobii.EyeTracking.IO;
using EyeTracker.Recorder;
using EyeTracker.Core;

namespace EyeTracker.Datasource
{
    class DataSourceSample : IDataSource
    {
        private delegate void AsyncMethodCaller();

        private SessionRecorder RecordedExperiment;
        private event EventHandler<TrackPoint> GeneratePointEvent;
        bool Subscribed = false;
        AsyncMethodCaller Caller = null;
        private Clock clock;

        public DataSourceSample(SessionRecorder RecordedExperiment, Clock clock)
        {
            this.RecordedExperiment = RecordedExperiment;
            this.clock = clock;
        }

        void IDataSource.Disconnect()
        {
            (this as IDisposable).Dispose();
        }

        private void GeneratePoints()
        {
            Thread.Sleep(100);
            foreach (var Point in RecordedExperiment.Points)
            {
                if (!Subscribed) return;
                GeneratePointEvent(this, Point);
                Thread.Sleep(50);
            }
        }

        void IDataSource.SubscribeListenerAndStartTracking(IListener Listener)
        {
            Subscribed = true;
            this.GeneratePointEvent += Listener.gazeDataReceived;
            if (Caller == null)
            {
                Caller = new AsyncMethodCaller(this.GeneratePoints);
                Caller.BeginInvoke(null, null);
            }
        }

        void IDataSource.StopTrackingAndUnsubscribeListener(IListener Listener)
        {
            Subscribed = false;
            if (Caller != null)
            {
                Caller = null;
            }
            this.GeneratePointEvent -= Listener.gazeDataReceived;
        }

        DataSourceTypeEnum IDataSource.GetDataSourceType()
        {
            return DataSourceTypeEnum.SIMULATION;
        }

        Calibration IDataSource.Calibrate()
        {
            return null;
        }

        bool IHasOwnProperties.HasOwnProperties()
        {
            return true;
        }

        SessionRecorder IHasOwnProperties.GetOwnProperties()
        {
            return RecordedExperiment;
        }

        void IDisposable.Dispose()
        {
            if (RecordedExperiment != null)
            {
                RecordedExperiment = null;
            }
        }

        public Clock GetClock()
        {
            return clock;
        }
    }
}
