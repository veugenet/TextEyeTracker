﻿using System;
using EyeTracker.Calib;
using Tobii.EyeTracking.IO;
using EyeTracker.Core;
using EyeTracker.Recorder;

namespace EyeTracker.Datasource
{
    public class DataSourceEyeTracker : IDataSource
    {
        private IEyeTracker ConnectedTracker;
        private ISyncManager SyncManager;
        private event EventHandler<TrackPoint> GeneratePointEvent;
        private TrackType.TrackEye TrackType;
        private Clock Clock;
        bool Subscribed = false;

        public DataSourceEyeTracker(EyeTrackerInfo ConnectedTracker, Clock Clock, TrackType.TrackEye TrackType)
        {
            this.ConnectedTracker = ConnectedTracker.Factory.CreateEyeTracker();
            this.ConnectedTracker.ConnectionError += HandleConnectionError;
            this.Clock = Clock;
            this.SyncManager = ConnectedTracker.Factory.CreateSyncManager(Clock);
            this.TrackType = TrackType;
            this.ConnectedTracker.GazeDataReceived += GazeDataRecieved;
        }

        private void HandleConnectionError(object sender, ConnectionErrorEventArgs e)
        {
            (this as IDataSource).Disconnect();
        }

        void IDataSource.Disconnect()
        {
            if (ConnectedTracker != null)
            {
                ConnectedTracker.Dispose();
                ConnectedTracker = null;

                SyncManager.Dispose();
                SyncManager = null;
            }
        }

        private void GazeDataRecieved(object sender, GazeDataEventArgs e)
        {
            IGazeDataItem gd = e.GazeDataItem;
            TrackPoint point = new TrackPoint(gd, Clock.Time);

            if (!Subscribed) return;
            GeneratePointEvent(this, point);

        }

        void IDataSource.SubscribeListenerAndStartTracking(IListener Listener)
        {
            Subscribed = true;
            this.GeneratePointEvent += Listener.gazeDataReceived;
            ConnectedTracker.StartTracking();
        }

        void IDataSource.StopTrackingAndUnsubscribeListener(IListener Listener)
        {
            Subscribed = false;
            ConnectedTracker.StopTracking();
            this.GeneratePointEvent -= Listener.gazeDataReceived;
        }

        DataSourceTypeEnum IDataSource.GetDataSourceType()
        {
            return DataSourceTypeEnum.EYETRACKER;
        }

        Calibration IDataSource.Calibrate()
        {
            var runner = new CalibrationRunner();
            return runner.RunCalibration(ConnectedTracker);
        }

        bool IHasOwnProperties.HasOwnProperties()
        {
            return false;
        }

        SessionRecorder IHasOwnProperties.GetOwnProperties()
        {
            return null;
        }

        void IDisposable.Dispose()
        {
            this.ConnectedTracker.Dispose();
            this.ConnectedTracker = null;
        }

        public Clock GetClock()
        {
            return Clock;
        }
    }
}
