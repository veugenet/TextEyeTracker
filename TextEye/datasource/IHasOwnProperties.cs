﻿using EyeTracker.Recorder;

namespace EyeTracker.Datasource
{
    public interface IHasOwnProperties
    {
        bool HasOwnProperties();

        SessionRecorder GetOwnProperties();
    }
}
