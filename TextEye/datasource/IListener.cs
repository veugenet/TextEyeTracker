﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tobii.EyeTracking.IO;
using EyeTracker.Core;

namespace EyeTracker.Datasource
{
    public interface IListener
    {
        void gazeDataReceived(object sender,TrackPoint e);
    }
}
