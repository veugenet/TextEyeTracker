﻿using Tobii.EyeTracking.IO;

namespace EyeTracker
{
    class EyeTrackerItem
    {
        public string Model { get; private set; }

        private EyeTrackerInfo info;

        public override string ToString()
        {
            return Model;
        }

        public EyeTrackerItem(EyeTrackerInfoEventArgs e)
        {
            Model = e.EyeTrackerInfo.Model;
            info = e.EyeTrackerInfo;
        }

        public EyeTrackerInfo GetInfo()
        {
            return info;
        }
    }
}
