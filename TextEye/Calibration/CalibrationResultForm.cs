﻿using System;
using System.Windows.Forms;

namespace EyeTracker.Calib
{
    public partial class CalibrationResultForm : Form
    {
        public CalibrationResultForm()
        {
            InitializeComponent();
        }

        public void SetPlotData(Tobii.EyeTracking.IO.Calibration calibration)
        {
            _leftPlot.Initialize(calibration.Plot);
            _rightPlot.Initialize(calibration.Plot);
        }

        private void _okButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
    }
}