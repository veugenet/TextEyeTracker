﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace eyeTracker
{
    public partial class PaddingDialog : Form
    {
        public bool Accept { get; private set; }

        public PaddingDialog()
        {
            InitializeComponent();
        }

        private void PaddingDialog_Shown(object sender, EventArgs e)
        {

        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            Accept = true;
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Accept = false;
            Close();
        }

        public Padding GetPadding()
        {
            Padding pd = new Padding((int)numPadLeft.Value, (int)numPadTop.Value, (int)numPadRight.Value, (int)numPadBottom.Value);
            return pd;
        }

        public void SetPadding(Padding pd)
        {
            numPadLeft.Value = pd.Left;
            numPadBottom.Value = pd.Bottom;
            numPadRight.Value = pd.Right;
            numPadTop.Value = pd.Top;
        }

        public int GetLineHeight()
        {
            return (int)numLineHeight.Value;
        }

        public void SetLineHeight(int value)
        {
            numLineHeight.Value = (decimal)value;
        }

    }
}
