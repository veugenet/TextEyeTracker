﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using Tobii.EyeTracking.IO;
using System.Diagnostics;
using EyeTracker.Recorder;
using EyeTracker.Core;
using EyeTracker;
using EyeTracker.Datasource;

namespace eyeTracker
{
    public partial class TextForm : Form, IListener
    { 
        private IDataSource connectedTracker;
        private SessionProperties sessionProps;
        private TextFormer textFormer;
        private SessionRecorder recorder;
        private int pageNumber = 0;
        private DateTime startTime;
        private Size? resize = null;
        Pen pointPen = new Pen(Color.Red);
        Graphics formGraphics;
        private Stopwatch Clock;
        private long totalPoints = 0;

        public TextForm(IDataSource tr, SessionProperties sessionProp, TextFormer textFormer)
        {
            InitializeComponent();

            this.sessionProps = sessionProp;
            this.textFormer = textFormer;

            connectedTracker = tr;
            if (tr.HasOwnProperties())
            {
                ApplyCompatibilityProperties(tr.GetOwnProperties());
            }
            else
            {
                this.Height = Screen.PrimaryScreen.Bounds.Height;
                this.Width = Screen.PrimaryScreen.Bounds.Width;
                this.WindowState = FormWindowState.Maximized;
            }

            if (resize != null)
            {
                this.Height = resize.Value.Height;
                this.Width = resize.Value.Width;
            }

            this.Clock = new Stopwatch();
            Clock.Start();

            formGraphics = this.CreateGraphics();
        }
        
        private void ApplyCompatibilityProperties(SessionRecorder Recorder)
        {
            resize = new Size(Recorder.ScreenResolution.Width, Recorder.ScreenResolution.Height);
            this.sessionProps.TextFont = Recorder.SessionProps.TextFont;
            this.sessionProps.TextPadding = Recorder.SessionProps.TextPadding;
            if(resize.Value.Width > Screen.PrimaryScreen.Bounds.Width ||
                resize.Value.Height > Screen.PrimaryScreen.Bounds.Height)
            {
                MessageBox.Show("Recorder experiment resolution is larger than your screen"
                    + "- you might not be able to see all points. Consider changing your screen resolution to the following:"
                    + System.Environment.NewLine + "Width:" + resize.Value.Width
                    + System.Environment.NewLine + "Height:" + resize.Value.Height, 
                    "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                throw new ArgumentException();
            }
        }

        private void TextForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            connectedTracker.StopTrackingAndUnsubscribeListener(this);
            String time = startTime.ToString();
            time = time.Replace(':', '-');
            this.Clock.Stop();

            if (connectedTracker.GetDataSourceType() == DataSourceTypeEnum.EYETRACKER)
                saveSessionData(sessionProps.OutputFolder + sessionProps.OutputFilePrefix + time + ".xml");
        }

        private void saveSessionData(String fileName)
        {
            recorder.Words = textFormer.WordList;
            recorder.PageInfo = textFormer.PageInfo;
            SessionSerializer.SerializeSession(recorder, fileName);
        }

        void IListener.gazeDataReceived(object sender, TrackPoint point)
        {
            Point2D p = point.RightGazePoint;
            switch (sessionProps.TrackType)
            {
                case TrackType.TrackEye.LeftEye:
                    p = point.LeftGazePoint;
                    break;
                case TrackType.TrackEye.RightEye:
                    p = point.RightGazePoint;
                    break;
                case TrackType.TrackEye.BothEye:
                    p.X = (point.LeftGazePoint.X + point.RightGazePoint.X) / 2;
                    p.Y = (point.LeftGazePoint.Y + point.RightGazePoint.Y) / 2;
                    break;
            }

            int px = (int)(this.Width * p.X);
            int py = (int)(this.Height * p.Y);

            if (sessionProps.TrackType != TrackType.TrackEye.None)
                formGraphics.DrawRectangle(pointPen, px - 2, py - 2, 4, 4);

            if (connectedTracker.GetDataSourceType() == DataSourceTypeEnum.EYETRACKER)
            {
                recorder.AddPont(point);
            }

            totalPoints++;

            if (connectedTracker.GetDataSourceType() == DataSourceTypeEnum.SIMULATION)
            {
                foreach (KeyValuePair<Int64, int> pg in connectedTracker.GetOwnProperties().PageTimer)
                    if (pg.Key == totalPoints) { pageNumber = pg.Value; Invoke((Action)delegate { Refresh(); }); }
            }

        }

        private void TextForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
            if ((e.KeyCode == Keys.Right || e.KeyCode == Keys.Space) && connectedTracker.GetDataSourceType() == DataSourceTypeEnum.EYETRACKER)
            {
                pageNumber++;
                if (pageNumber > textFormer.PageInfo.Count - 1)
                    pageNumber = textFormer.PageInfo.Count - 1;
                else
                    recorder.PageTimer.Add(totalPoints, pageNumber);
                Refresh();
            }
            if (e.KeyCode == Keys.Left && connectedTracker.GetDataSourceType() == DataSourceTypeEnum.EYETRACKER)
            {
                pageNumber--;
                if (pageNumber < 0)
                    pageNumber = 0;
                else
                    recorder.PageTimer.Add(totalPoints, pageNumber);
                Refresh();
            }
        }

        private void TetxForm_Paint(object sender, PaintEventArgs e)
        {
            List<PageInfo> pginfo = textFormer.PageInfo;
            List<Word> words = textFormer.WordList;
            
            PageInfo pg = pginfo[pageNumber];

            Graphics gr = this.CreateGraphics();
            SolidBrush drawBrush = new SolidBrush(sessionProps.TextColor);
            gr.Clear(sessionProps.BackgroungColor);

            for(int i = pg.StartWord-1; i <= pg.EndWord-1; i++)
            {
                PointF pt = new PointF(words[i].Coord.Left, words[i].Coord.Top);
                gr.DrawString(words[i].ThisWord, sessionProps.TextFont, drawBrush, pt);
                if (sessionProps.TrackType != TrackType.TrackEye.None)
                {
                    Pen pen = new Pen(drawBrush);
                    gr.DrawRectangle(pen, words[i].Coord);
                }
            }
        }

        private void TextForm_Shown(object sender, EventArgs e)
        {
            startTime = DateTime.Now;

            recorder = new SessionRecorder();
            recorder.SessionProps = sessionProps;
            recorder.StartClockTime = connectedTracker.GetClock().Time;
            connectedTracker.SubscribeListenerAndStartTracking(this);
        }
    }
}
