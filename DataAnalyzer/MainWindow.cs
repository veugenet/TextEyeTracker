﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using EyeTracker;
using EyeTracker.Recorder;
using DataAnalyzer.Algorithms;

namespace DataAnalyzer
{
    public partial class MainWindow : Form
    {
        SessionRecorder recorder = null;
        String fileName;

        public MainWindow()
        {
            InitializeComponent();
            TrackType t1 = new TrackType(TrackType.TrackEye.BothEye);
            TrackType t2 = new TrackType(TrackType.TrackEye.LeftEye);
            TrackType t3 = new TrackType(TrackType.TrackEye.RightEye);
            cmbTrackType.Items.Add(t1);
            cmbTrackType.Items.Add(t2);
            cmbTrackType.Items.Add(t3);
            cmbTrackType.SelectedIndex = 0;
            cmbAlgorithm.SelectedIndex = 0;

            updateUI();
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            openFileDlg.Filter = "XML|*.xml";
            if (openFileDlg.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    fileName = openFileDlg.FileName;
                    recorder = SessionSerializer.DeserializeSession(fileName);
                    updateUI();
                }
                catch
                {
                    MessageBox.Show("Error during file loading!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void updateUI()
        {
            if (recorder != null)
            {
                btnAnalyze.Enabled = true;
                cmbAlgorithm.Enabled = true;
                cmbTrackType.Enabled = true;

                txtFileName.Text = fileName;

                String info = "Words: " + recorder.Words.Count + "\n" +
                    "Points: " + recorder.Points.Count + "\n";

                rtbInfo.Text = info;
            }
            else
            {
                btnAnalyze.Enabled = false;
                cmbAlgorithm.Enabled = false;
                cmbTrackType.Enabled = false;
            }
        }

        private void miExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnAnalyze_Click(object sender, EventArgs e)
        {
            saveFileDlg.Filter = "CSV|*.csv";
            IAnalyzeAlgorithm alg = null;
            switch (cmbAlgorithm.SelectedIndex)
            {
                case 0:
                    alg = new SimpleAlgorithm(recorder); 
                    break;
                case 1:
                    alg = new ModeSimpleAlgorithm(recorder);
                    break;
            }
            if (alg != null && saveFileDlg.ShowDialog() == DialogResult.OK)
            {
                alg.Analyze((TrackType)cmbTrackType.SelectedItem);
                alg.SaveToCsv(saveFileDlg.FileName);
            }
        }
    }
}
