﻿using System;
using System.Collections.Generic;
using System.Linq;
using EyeTracker;
using EyeTracker.Recorder;
using EyeTracker.Core;

namespace DataAnalyzer.Algorithms
{
    class SimpleAlgorithm: IAnalyzeAlgorithm
    {
        protected SessionRecorder recorder;
        protected List<AlgorithmResult> result = new List<AlgorithmResult>();

        public SimpleAlgorithm(SessionRecorder recorder)
        {
            this.recorder = recorder;
        }

        protected void simpleAnalyse(TrackType trackType)
        {
            int pointsCount = 0;
            int page = 0;
            foreach (TrackPoint point in recorder.Points)
            {
                int gazeX = 0;
                int gazeY = 0;
                switch (trackType.GetTrackEye())
                {
                    case TrackType.TrackEye.BothEye:
                        double px = (point.LeftGazePoint.X + point.RightGazePoint.X) / 2;
                        double py = (point.LeftGazePoint.Y + point.RightGazePoint.Y) / 2;
                        gazeX = (int)(recorder.ScreenResolution.Width * px);
                        gazeY = (int)(recorder.ScreenResolution.Height * py);
                        break;
                    case TrackType.TrackEye.LeftEye:
                        gazeX = (int)(point.LeftGazePoint.X * recorder.ScreenResolution.Width);
                        gazeY = (int)(point.LeftGazePoint.Y * recorder.ScreenResolution.Height);
                        break;
                    case TrackType.TrackEye.RightEye:
                        gazeX = (int)(point.RightGazePoint.X * recorder.ScreenResolution.Width);
                        gazeY = (int)(point.RightGazePoint.Y * recorder.ScreenResolution.Height);
                        break;
                }

                pointsCount++;
                foreach (KeyValuePair<Int64, int> pg in recorder.PageTimer)
                {
                    if (pg.Key == pointsCount)
                    {
                        page = pg.Value;
                        break;
                    }
                }
                List <Word> wd = recorder.Words.GetRange(recorder.PageInfo[page].StartWord-1, recorder.PageInfo[page].EndWord-recorder.PageInfo[page].StartWord-1);

                foreach (Word w in wd)
                {
                    if (gazeX > w.Coord.X && gazeX < (w.Coord.X + w.Coord.Width))
                        if (gazeY > w.Coord.Y && gazeY < (w.Coord.Y + w.Coord.Height))
                        {
                            addPoint(w, point.Time);
                            break;
                        }
                }
            }
        }

        public virtual void Analyze(TrackType trackType)
        {
            simpleAnalyse(trackType);
        }

        protected void addPoint(Word word, long pointTime)
        {
            if (result.Count == 0)
            {
                result.Add(new AlgorithmResult(word, pointTime));
                return;
            }
            if (result.Last().GetWord() == word)
            {
                
                result.Last().IncPointsCount(pointTime);
            }
            else
                result.Add(new AlgorithmResult(word, pointTime));
        }

        public virtual bool SaveToCsv(String fileName)
        {
            try
            {

                System.IO.StreamWriter file = new System.IO.StreamWriter(fileName, false/*, Encoding.Unicode*/);
                file.WriteLine("#;ID;WORD;POINTS;TIME");
                int i = 1;
                foreach (AlgorithmResult res in result)
                {
                    file.WriteLine(i.ToString() + ";" + res.ToString());
                    i++;
                }
                file.Close();
            }
            catch(System.IO.IOException)
            {
                return false;
            }
            return true;
        }

        public override string ToString()
        {
            return "Simple algorithm";
        }
    }
}
