﻿using EyeTracker.Core;

namespace DataAnalyzer.Algorithms
{
    class AlgorithmResult
    {
        private Word word;
        private int pointsCount;
        private long lastTime;
        private long totalTime;

        public AlgorithmResult(Word word, long time)
        {
            this.word = word;
            pointsCount = 1;
            totalTime = 0;
            lastTime = time;
        }

        public void IncPointsCount(long time)
        {
            pointsCount++;
            totalTime += time - lastTime;
            lastTime = time;
        }

        public Word GetWord()
        {
            return word;
        }

        public int GetPointsCount()
        {
            return pointsCount;
        }

        public long GetTotalTime()
        {
            return totalTime;
        }

        public void AddPoints(int count, long time)
        {
            pointsCount += count;
            totalTime += time;
        }

        public override string ToString()
        {
            return (word.ID+1).ToString() + ";" + word.ThisWord + ";" + pointsCount.ToString() + ";" + totalTime.ToString();
        }

        public static AlgorithmResult operator+(AlgorithmResult r1, AlgorithmResult r2)
        {
            if (!r1.Equals(r2)) throw new System.ArgumentException();
            AlgorithmResult res = new AlgorithmResult(r1.GetWord(), 0);
            res.AddPoints(r1.GetPointsCount() + r2.GetPointsCount(), r1.GetTotalTime() + r2.GetTotalTime());
            return res;
        }

        public override bool Equals(System.Object obj)
        {
            if (obj == null)
            {
                return false;
            }
            AlgorithmResult alr = obj as AlgorithmResult;
            if ((System.Object)alr == null)
                return false;
            return (word.ID == alr.GetWord().ID);
        }
    }
}
