﻿using EyeTracker.Recorder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAnalyzer.Algorithms
{
    class ModeSimpleAlgorithm: SimpleAlgorithm
    {
        public ModeSimpleAlgorithm(SessionRecorder recorder): base(recorder)
        {

        }
        public override bool SaveToCsv(String fileName)
        {
            try
            {
                System.IO.StreamWriter file = new System.IO.StreamWriter(fileName, false/*, Encoding.Unicode*/);
                file.WriteLine("#;ID;WORD;POINTS;TIME");
                int i = 1;
                foreach (AlgorithmResult res in result)
                {
                    if (res.GetPointsCount() > 2)
                    {
                        file.WriteLine(i.ToString() + ";" + res.ToString());
                        i++;
                    }
                }
                file.Close();
            }
            catch (System.IO.IOException)
            {
                return false;
            }
            return true;
        }

        private void optimize()
        {
            List<AlgorithmResult> optList = new List<AlgorithmResult>();
            foreach (AlgorithmResult res in result)
            {

            }
        }

    }
}
