﻿using System;

namespace DataAnalyzer.Algorithms
{
    interface IAnalyzeAlgorithm
    {
        void Analyze(EyeTracker.TrackType trackType);
        bool SaveToCsv(String fileName);
    }
}
