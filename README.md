Eye tracker experiment
===================
----------


Dependencies
-------------
Visual Studio 2015

Other dependencies:
> - Tobii Eye Tracker SDK

Building
-------------
1. Create blank solution
2. Add projects from repository to solution
3. Download dependencies archive
4. Unpack archive to projects upper directory (eyetrackerexperiment\packages)
5. Set icons from packages directory to projects
